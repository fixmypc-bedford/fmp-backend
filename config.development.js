const App = require('./lib/App.js');
const TabGroup = require('./lib/TabGroup.js');
const Tab = require('./lib/Tab.js');
const Icon = require('./lib/Icon.js');
const Widget = require('./lib/Widget.js');

let storeGroup = new TabGroup("",[
    new Tab("http://192.168.1.65/pcrt/repair", new Icon("icon-wrench"), "Booking System", "#e74c3c", new Widget("http://192.168.1.65/pcrt/repair/sidemenu.php?inline=true", 600)),
    new Tab("http://192.168.1.65/pcrt/store", new Icon("icon-store"), "Point of Sale", "#e74c3c"),
    new Tab("fmpb://html/pricing.html", new Icon("icon-coin-pound"), "Pricing List", "#2a5298"),
]);

let internalGroup = new TabGroup("Internal Tools",[
    new Tab("fmpb://html/sticky-notes.html", new Icon("icon-calendar-full"), "Sticky Notes", "#e74c3c"),
    new Tab("https://stats.uptimerobot.com/JQymku9kL", new Icon("icon-file-stats"), "Service Status", "#4ba74b"),
    new Tab("https://trello.com/fixmypc", new Icon("fmpb://img/trello.svg", "image"), "Trello", "#026aa7"),
    new Tab("fmpb://html/label.html", new Icon("fmpb://img/brother-1.svg", "image"), "Label Printer", "#0d2ea0"),
]);

let warrantyGroup = new TabGroup("Useful Websites", [
    new Tab("http://www.dell.com/support/home/uk/en/ukbsdt1?app=warranty", new Icon("fmpb://img/Dell_Logo.svg","image"), "Dell Warranty", "#007db8"),
    new Tab("https://checkcoverage.apple.com/gb/en/", new Icon("icon-apple2"), "Apple Warranty", "#333"),
    new Tab("https://support.hp.com/us-en/checkwarranty", new Icon("fmpb://img/HP_New_Logo_2D.svg", "image"), "HP Warranty", "#0096D6"),
    new Tab("https://support.toshiba.com/warranty", new Icon("fmpb://img/Toshiba_logo.svg", "image"), "Toshiba Warranty", "#f00"),
    new Tab("https://www.amazon.co.uk/", new Icon("icon-amazon"), "Amazon UK", "#232f3e"),
    new Tab("https://www.ebay.co.uk/", new Icon("icon-ebay"), "Ebay UK", "linear-gradient(to right, #ff3232 0%,#ff3030 25%,#0063d1 25%,#0063d1 50%,#f4ae01 50%,#f4ae01 50%,#f4ae01 75%,#85b716 75%,#85b716 100%) no-repeat"),
    new Tab("https://www.thebookyard.com/",new Icon("fmpb://img/bookyard.png", "image"), "TheBookYard", "#333"),
    new Tab("http://support.seagate.com/apps/warranty",new Icon("fmpb://img/seagate.svg", "image"), "Seagate", "#72C04F")
]);

let supportGroup = new TabGroup("Support", [
    new Tab("https://discordapp.com/channels/389628454800457739/389631578105708554", new Icon("fmpb://img/discord.svg", "image"), "Discord", "#7289da"),
    new Tab("https://www.facebook.com/FixMyPCBedford/", new Icon("icon-facebook2"), "Facebook", "#3b5998"),
    new Tab("https://twitter.com/FixMyPCBedford/", new Icon("icon-twitter"), "Twitter", "#1DA1F2"),
    new Tab("https://webmail.fixmy-pc.co.uk/", new Icon("icon-envelope"), "Webmail", "#e74c3c"),
]);

let passwordGroup = new TabGroup("Password Recovery",[
    new Tab("https://accounts.google.com/signin/recovery", new Icon("icon-google"), "Google", "#EEB211"),
    new Tab("https://account.live.com/password/reset", new Icon("icon-microsoft"), "Microsoft", "#00a1f1"),
    new Tab("https://login.yahoo.com/account/challenge/username", new Icon("icon-yahoo"), "Yahoo", "#3e00a1"),
]);

App.addGroup(storeGroup);
App.addGroup(internalGroup);
App.addGroup(supportGroup);
App.addGroup(warrantyGroup);
App.addGroup(passwordGroup);

App.setServiceName("FixMyPC Development");

App.setWallpaper("fmpb://img/104a.jpg");

App.setLabelServer({
    host: '192.168.1.166',
    port: 8090,
    version: 1
});

App.setMode("development");
