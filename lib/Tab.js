class Tab {
    constructor(u, i, t, c, w) {

        this.url = "http://google.com/";
        this.icon = "";
        this.colour = "red";
        this.title = "Google Homepage";
        this.widget = null;

        if (typeof u != "undefined") this.url = u;
        if (typeof i != "undefined") this.icon = i;
        if (typeof t != "undefined") this.title = t;
        if (typeof c != "undefined") this.colour = c;
        if (typeof w != "undefined") this.widget = w;
    }
    setURL(u) {
        this.url = u;
    }
    setIcon(i) {
        this.icon = i;
    }
    setTitle(t) {
        this.title = t;
    }
    setColour(c) {
        this.colour = c;
    }
    setWidget(w) {
        this.widgetURL = w;
    }
}
module.exports = Tab;