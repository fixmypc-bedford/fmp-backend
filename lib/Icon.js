class Icon {
    constructor(n, t, c) {

        this.name = "";
        this.type = "icomoon";
        this.color = "white";

        /*
         * Name is either the icon class name or a url
         * 
         * Type can be either `icomoon` or `image`
         *  icomoon = Custom Icon from icomoon
         *  image = Relative/Absolute path to image file.
         *      Relative images are stored in "static/images" and accessed from "static/images" externally.
         */

        if (typeof t != "undefined") this.type = t;
        if (typeof n != "undefined") this.name = n;
        if (typeof c != "undefined") this.color = c;
    }
    setName(n) {
        this.name = n;
    }
    setType(t) {
        this.type = t;
    }
    setColor(c) {
        this.color = c;
    }
}
module.exports = Icon;