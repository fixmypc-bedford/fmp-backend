class Widget {
    constructor(url, height) {
        this.url = "";
        this.height = 100;

        if (typeof url !== "undefined") this.url = url;
        if (typeof height !== "undefined") this.height = height;
    }

    getURL() {
        return this.url;
    }
    getHeight() {
        return this.height;
    }

    setURL(u) {
        this.url = u;
    }
    setHeight(h) {
        this.height = h;
    }
}
module.exports = Widget;