class App {

    constructor() {
        this.groups = [];
        this.labelServer = {};
        this.serviceName = "FixMyPC Backend Devel";
        this.wallpaper = "";
        this.mode = "production";
    }

    addGroup(group) {
        this.groups.push(group);
    }

    setLabelServer(srv) {
        this.labelServer = srv;
    }

    setServiceName(name) {
        this.serviceName = name;
    }

    setWallpaper(url) {
        this.wallpaper = url;
    }

    setMode(mode){
        this.mode = mode;
    }

}
module.exports = new App();