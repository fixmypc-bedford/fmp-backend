class TabGroup {
    constructor(n, t) {
        this.name = "Tab Group";
        this.tabs = [];

        if (typeof n != "undefined") { this.name = n; }
        if (typeof t != "undefined") { this.tabs = t; }
    }
    setName(n) {
        this.name = n;
    }
    addTab(t) {
        this.tabs.push(t);
    }
}
module.exports = TabGroup;