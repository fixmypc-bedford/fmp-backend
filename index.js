// FixMyPC APP Backend
// Uses UDP Broadcasting to advertise its existence and secure a connection with the frontend app.
// Allows it to run anywhere on the network with zero configuration.

require('dotenv').config();

// HTTP API
const Hapi = require('hapi');
const Joi = require('joi');
const Boom = require('boom');
const Inert = require('inert');

// Socket.IO
const io = require('socket.io');

// Broadcast the backend
const dgram = require('dgram');
const broadcastServer = dgram.createSocket('udp4');
const Netmask = require('netmask').Netmask
const os = require('os');
const ifaces = os.networkInterfaces();
const path = require('path');
const uuid = require('uuid/v4')();

const environment = (process.env.NODE_ENV || "production").toLowerCase();

require(`./config.${environment}.js`);

const App = require('./lib/App.js');

console.log(`Starting backend in ${environment} mode.`);

const server = Hapi.Server({
    port: 7090
});
let websock = io(server.listener);
let chatLog = [];
websock.sockets.on('connection', (socket) => {
    socket.emit('ack',{ msg: 'welcome' });
    socket.emit('notification',{
        title: 'Connected!',
        message: 'Successfully connected to the backend!',
        color: "#2ecc71",
        timeout: 5000
    });
    
    socket.on('auth', data => {
        console.log("Socket authorised", data);
        socket.session = data;
        socket.emit('chat-history', chatLog);
    });

    socket.on('chat', data => {
        if (typeof socket.session === "undefined") return;
        
        console.log("Got chat message, redistributing..", data);
        let d = {
            message: data.toString(),
            username: socket.session.details.username,
        };
        chatLog.push(d);
        
        let notif = {
            title: 'Internal Chat',
            message: socket.session.details.username + ": " + data.toString(),
        }
        websock.sockets.emit('notification',notif);

        websock.sockets.emit('chat', d);
    });

    socket.on('subscribe', data => {
        // Socket subscribing to an event.
        console.log("A socket has subscribed to an event ", data);
        socket.subscribed.push(data.toLowerCase());
    });
});
websock.on('connection', (socket) => {
    console.log("Got websock conn");
})

// Start HTTPD
server.start().then(()=>{
    console.log(`Started HTTPD on port ${server}`, server.listener);

    server.register(Inert);

    server.route({
        method: 'GET',
        path: '/',
        handler: (req, h) => {
            return "Johnny-Five Alive! You're staring at the FMP Desktop Backend. Look with your eyes and not your hands!";
        }
    });

    server.route({
        method: 'PUT',
        path: '/notification',
        options: {
            validate: {
                payload: {
                    title: Joi.string().required(),
                    message: Joi.string().required(),
                    color: Joi.string().optional(),
                    timeout: Joi.number().optional(),
                },
            },
        },
        handler: (req, h) => {
            let notif = {};

            if (typeof req.payload.timeout !== "undefined") notif.timeout = req.payload.timeout;
            if (typeof req.payload.color !== "undefined") notif.color = req.payload.color;

            notif.title = req.payload.title;
            notif.message = req.payload.message;

            websock.sockets.emit('notification',notif);
            
            return "STATUS_OK";
        }
    });

    server.route({
        method: 'GET',
        path: '/config',
        handler: (h) => {
            return App;
        }
    });
    server.route({
        method: 'GET',
        path: '/assets/{param*}',
        handler: {
            directory: {
                path: path.join(__dirname, 'assets'),
                redirectToSlash: true,
                index:true,
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/chat/history',
        handler: (req, h) => {
            return chatLog;
        }
    });
});

var PORT = 6024;

broadcastServer.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

broadcastServer.on('message', (msg, rinfo) => {
  console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
});

broadcastServer.on('listening', () => {
  const address = broadcastServer.address();
  console.log(`Service Server listening ${address.address}:${address.port}`);
});

/* Broadcast Service Every 2 seconds */
broadcastServer.bind(()=>{
    broadcastServer.setBroadcast(true);
    //setInterval(broadcastNew, 2 * 1000);
});

// Broadcasts on all interfaces.
function broadcastNew() {

    for (iface in ifaces) {
        console.log(`Advertising Service on ${iface}`);
        for (let ip of ifaces[iface]) {
            if (ip.family.toLocaleLowerCase() == "ipv4") {
                let block = new Netmask(ip.cidr);
                var message = new Buffer(JSON.stringify(
                    {
                        id: uuid,
                        service: {
                            id: 'fmp-backend',
                            name: App.serviceName,
                        },
                        data: {
                            address: ip.address,
                            port: 7090,
                            channel: environment
                        },
                        type: 'advertise',
                    }
                ));
                broadcastServer.send(message, 0, message.length, PORT, block.broadcast, function() {
                    console.log(`Sent '${message}' on IPv4 ${ip.address}:${PORT}`);
                });

            }
        }
    }

}